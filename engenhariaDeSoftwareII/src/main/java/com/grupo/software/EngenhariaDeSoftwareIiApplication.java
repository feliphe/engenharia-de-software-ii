package com.grupo.software;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EngenhariaDeSoftwareIiApplication {

	public static void main(String[] args) {
		SpringApplication.run(EngenhariaDeSoftwareIiApplication.class, args);
	}

}
