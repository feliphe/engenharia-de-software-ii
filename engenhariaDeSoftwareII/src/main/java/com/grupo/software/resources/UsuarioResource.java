package com.grupo.software.resources;

import java.net.URI;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.grupo.software.domain.Usuario;
import com.grupo.software.dto.UsuarioDTO;
import com.grupo.software.dto.UsuarioNewDTO;
import com.grupo.software.services.UsuarioService;

@RestController
@RequestMapping(value = "/usuarios")
public class UsuarioResource {

	@Autowired
	private UsuarioService usuarioService;

	@RequestMapping(value = "/email", method = RequestMethod.GET)
	public ResponseEntity<?> buscarPorEmail(@RequestParam(value = "value") String email) {

		Usuario obj = usuarioService.findByEmail(email);
		UsuarioDTO objDTO = new UsuarioDTO(obj);

		return ResponseEntity.ok().body(objDTO);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> buscar(@PathVariable Integer id) {

		Usuario obj = usuarioService.findById(id);
		UsuarioDTO objDTO = new UsuarioDTO(obj);

		return ResponseEntity.ok().body(objDTO);
	}

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Void> insert(@Valid @RequestBody UsuarioNewDTO objDTO) {

		Usuario usuario = new Usuario(null, objDTO.getEmail(), objDTO.getSenha(), objDTO.getCpfOuCnpj());
		usuario = usuarioService.insert(usuario);

		URI uri = ServletUriComponentsBuilder
				               .fromCurrentRequestUri()
				               .path("/{id}")
				               .buildAndExpand(usuario.getId())
				               .toUri();

		return ResponseEntity.created(uri).build();
	}

}
