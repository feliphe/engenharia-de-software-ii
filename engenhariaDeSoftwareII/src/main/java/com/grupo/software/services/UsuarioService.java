package com.grupo.software.services;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.grupo.software.domain.Usuario;
import com.grupo.software.repositories.UsuarioRepository;
import com.grupo.software.services.exception.ObjectNotFoundException;

@Service
public class UsuarioService {

	@Autowired
	private UsuarioRepository usuarioRepo;
	
	public Usuario findByEmail(String email) {
		
		Optional<Usuario> obj = usuarioRepo.findByEmail(email);
		
		return obj.orElseThrow(() -> 
			new ObjectNotFoundException("Objeto não encontrado! email: " + email + ", Tipo: " + Usuario.class.getName()));
	}
	
	public Usuario findById(Integer id) {
		
		Optional<Usuario> obj = usuarioRepo.findById(id);
		
		return obj.orElseThrow(() -> 
			new ObjectNotFoundException("Objeto não encontrado! id: " + id + ", Tipo: " + Usuario.class.getName()));
	}
	
	@Transactional 
	public Usuario insert(Usuario obj) {
		
		obj.setId(null); obj = usuarioRepo.save(obj); 
		return obj;
	}
}
